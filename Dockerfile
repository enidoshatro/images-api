FROM ruby:2.7.0

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev imagemagick
 # The following are used to trim down the size of the image by removing unneeded data
RUN apt-get clean autoclean \
  && apt-get autoremove -y \
  && rm -rf \
    /var/lib/apt \
    /var/lib/dpkg \
    /var/lib/cache \
    /var/lib/log

RUN mkdir /app
WORKDIR /app

COPY Gemfile /app/Gemfile
COPY Gemfile.lock /app/Gemfile.lock

RUN bundle install

COPY . /app