# README

## Ruby version `2.7.0`

## System dependencies `Docker` & `Docker-compose` 

## First build
> docker-compose build

## Run
> docker-compose up -d

## Local dev link > 0.0.0.0:3000

## Routes @ ./config/routes.rb
> GET POST - /items
> GET /items/images
> GET /items/:id
> PUT /items/:id
> DELETE /sources/:id

## Database creation by default it will create a db without pass

## Database initialization 
> docker-compose run web rake db:create

> docker-compose exec web rails db:migrate

## How to run the test suite
> docker-compose exec web rails test

## Services (job queues, cache servers, search engines, etc.)

## Deployment instructions

