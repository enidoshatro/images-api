class CreateItems < ActiveRecord::Migration[6.0]
  def change
    create_table :items do |t|
      t.uuid :uuid
      t.string :title
      t.string :description
      t.string :image

      t.timestamps
    end
  end
end
