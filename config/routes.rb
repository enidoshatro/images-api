Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      resources :items do
        post 'import', on: :collection
        get 'images', on: :collection
      end
      resources :sources
    end
  end
end
