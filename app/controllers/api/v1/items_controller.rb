class Api::V1::ItemsController < ApplicationController
  # GET /items
  def index
    @items = Item.all
    render json: @items

  end

  # POST /items
  def create
    @item = Item.new({uuid: SecureRandom.uuid, **item_params})
    unless @item.save
      render json: {status: 'ERROR', message: 'Item not saved', data: @item.errors}, status: :unprocessable_entity
    end

    render json: {status: 'SUCCESS', message: 'Saved item', data: @item}, status: :created
  end

  # POST /items/import
  def import
    csv_parser_service = CsvParser.new
    image_service = Images.new

    result = csv_parser_service.fetch_remote_resource
    csv_data = csv_parser_service.init_csv_values(result)
    data_with_uuid = prepare_items(csv_data)
    image_service.fetch_images(data_with_uuid)

    @items = Item.create(data_with_uuid) do |item|
      url = request.url.sub "import", "images"
      item.image = "#{url}?uuid=#{item.uuid}"
    end
    render json: {status: 'Success', data: @items}, status: :ok
  end

  # GET /items/images
  def images
    params.require(:uuid)
    uuid = params[:uuid]
    render send_file("#{Rails.root}/public/images/thumbnail_#{uuid}.jpeg",
                     filename: "thumbnail_#{uuid}.jpeg",
                     type: "image/jpeg")
  end

  # GET /items/:id
  def show
    render json: @item
  end

  # PUT /items/:id
  def update
    unless @item
      render json: {success: "false"}, status: 200
    end

    @item.update(item_params)
    render json: {success: 'true'}, status: 200
  end

  # DELETE /sources/:id
  def destroy
    unless @item
      render json: {success: "false"}, status: 200
    end

    @item.destroy
    render json: {success: 'true'}, status: 200
  end

  private

  def item_params
    params.permit(:title, :description, :image)
  end

  def find_item
    @item = Item.find(params[:id])
  end

  protected
  def prepare_items(data)
    data.map { |item| {"uuid" => SecureRandom.uuid, **item} }
  end
end