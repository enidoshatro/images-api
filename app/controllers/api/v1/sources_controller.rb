require 'securerandom'

class Api::V1::SourcesController < ApplicationController

  # GET /sources
  def index
    @sources = Source.all
    render json: @sources
  end

  # GET /sources/:id
  def show
    @source = Source.find(params[:id])
    render json: @source
  end

  # Post /sources
  def create
    @source = Source.new({url: params[:url]})

    unless @source.save
      render json: {status: 'ERROR', message:'Source not saved', data: @source.errors}, status: :unprocessable_entity
    end
    #@sources = Source.all
    #redirect_to(index)
    render json: @source
  end

  # PUT /sources/:id
  #def put
  #  @sources = Source.all
  #  render json: @sources
  #end

  # DELETE /sources/:id
  def destroy
    @source = Source.find(params[:id])
    unless @source
      render json: {success: "false"}, status: 200
    end

    @source.destroy
    render json: {success: 'true'}, status: 200
  end

  private

  def source_params
    params.permit(:url)
  end
end
