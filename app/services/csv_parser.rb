class CsvParser
  URL = "https://docs.google.com/spreadsheets/d/10Rl6me8gKbLufgUq5kS6aqfd6dZIj3__8OfjOrlKM2Y/export?format=csv&id=10Rl6me8gKbLufgUq5kS6aqfd6dZIj3__8OfjOrlKM2Y&gid=0"

  def fetch_remote_resource(address_url = URL)
    response = HTTParty.get(address_url)
    if response.code != 200
      return nil
    end

    response.body
  end

  def init_csv_values(content)
    CSV.parse(content, headers: :first_row).map(&:to_h)
  end
end