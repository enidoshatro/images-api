class Images
  def is_downloadable(response)
    if response.code != 200
      return false
    end

    headers = response.headers
    content_type = headers["Content-Type"]

    if "text".in? content_type
      return false
    end

    if "html".in? content_type
      return false
    end

    content_length = headers["Content-Length"]
    if content_length and content_length.to_i > 2e8 # 200 mb
      return false
    end

    unless "jpeg".in? content_type
      return false
    end

    true
  end

  def fetch_and_save_image(key, content)
    path = File.join Rails.root, 'public', 'images'
    return if content.empty?
    FileUtils.mkdir_p(path) unless File.exist?(path)

    puts File.join(path, "thumbnail_#{key}.jpeg")

    File.open(File.join(path, "thumbnail_#{key}.jpeg"), 'wb') do |file|
      file.puts content
    end

    image = MiniMagick::Image.new(File.join(path, "thumbnail_#{key}.jpeg"))
    image.resize "150x150"
  end

  def fetch_images(items)
    hydra = Typhoeus::Hydra.new
    items.map do |item|

      puts item

      if item["image"]
        request = Typhoeus::Request.new(item["image"], followlocation: true)
        hydra.queue(request)
        request.on_complete do |response|
          if is_downloadable(response)
            fetch_and_save_image(item["uuid"], response.body)
          end
        end
      end
    end
    hydra.run
  end
end